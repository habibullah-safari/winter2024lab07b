import java.util.Random; 
public class Deck{
	private Card[] cards;
	private int numberOfCards ;
	private Random rng;
	
	//constructor
	public Deck(){
		Card [] cardArray = new Card[52];
		this.cards = cardArray; 
		this.numberOfCards =52;
		this.rng = new Random(); 
		
		// the process of putting the cards with their specific suits in the deck of cards
		int counter =0;
		for(int index =0; index < Suits.values().length; index++){	
			Suits newSuit = Suits.values()[index];
			for (int j = 0; j < Ranks.values().length; j++){
					Ranks newRank = Ranks.values()[j];
					this.cards[counter] = new Card( newRank, newSuit);	
						counter++;
			}
		}	
	}
	//Getters 
	public Card[] getCards(){
		return this.cards;
	}
	public int getNumberOfCards(){
		return this.numberOfCards;
	}
	//length method	
	public int length(){
		return this.numberOfCards;
	}
	// Draw a card
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	// To String 
	public String toString(){			
		String allCards= ""; 
		for(int index =0 ; index < this.numberOfCards; index++){
			 allCards += cards[index] + "\n";  
		}
	return allCards;
	}
	// returns a String which contains all the cards inside the deck
	//"\n" makes new 
	// Shuffle method
	public void shuffle(){
		for(int index = 0; index < this.numberOfCards; index++){
			int randomnumber = rng.nextInt(numberOfCards) ;
			Card swap = this.cards[index];
			this.cards[index] = this.cards[randomnumber]; 
			this.cards[randomnumber] = swap;
		}
	}
}