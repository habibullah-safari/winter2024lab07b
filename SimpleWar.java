public class SimpleWar{
    public static void main(String [] args){

        Deck newDeck = new Deck();
		//System.out.println("Before Shuffle" + "\n" + newDeck);
        newDeck.shuffle();
        //System.out.println("After Shuffle" + "\n" +newDeck);

        Double p1Points = 0.0;
        Double p2Points = 0.0; 

		// Two Players draw cards 

		//player one 
		int round = 1;
		while (newDeck.getNumberOfCards() > 0){
			System.out.println( "\033[0;34m"  + " Round " + round + "\u001B[0m" );
			Card p1Card = newDeck.drawTopCard();
			System.out.println(p1Card);
			double p1Score = p1Card.calculateScore();
			System.out.println(p1Score);
			
			//Player Two
			Card p2Card = newDeck.drawTopCard();
			System.out.println(p2Card);
			double p2Score = p2Card.calculateScore();
			System.out.println(p2Score);
			
			System.out.println(" player 1 points before this round " + p1Points);
			System.out.println(" player 2 points before this round " + p2Points);

			//who wins this round the point will be added to their points
			if ( p1Score > p2Score){
				System.out.println("Player 1 wins this round");
				p1Points++;
			}else{
				System.out.println("Player 2 wins this round");
					p2Points++;
			}
			
			System.out.println(" player 1 points after this round " + p1Points + "\u001B[0m");
			System.out.println(" player 2 points after this round " + p2Points + "\u001B[0m") ;
			System.out.println("\u001B[36m" + "<============================================>" + "\u001B[0m");
			round++;
		}
		
		if (p1Points > p2Points){
			System.out.println( " \033[0;35m" + "Player 1 WON the game congratulations " + "\u001B[0m");	
		}else if (p2Points > p1Points){
			System.out.println(" \033[0;35m" + "Player 2 WON the game congratulations " + "\u001B[0m");	
		}else{
			System.out.println( " \033[0;35m" + " It's a TIE congratulations to both players " + "\u001B[0m");	
		}
    }
}