public enum Suits{

    Hearts(0.4),
    Spades(0.3),
    Diamonds(0.2),
    Clubs(0.1) ;

    private final double score;
    
	//constructor
    private Suits( double score){
        this.score = score;
    }
	//getter 
	public double getScore(){
		return this.score;
	}

}