public class Card{
	private Ranks ranks;
	private Suits suits;

	// constructor
	public Card(Ranks ranks, Suits suits){
		this.ranks = ranks;
		this.suits = suits;
	}
	//Getters
	public Ranks getRank(){
		return this.ranks;
	}
	public Suits getSuit(){
		return this.suits;
	}
	//toString
	public String toString(){
		return "The card is " + this.ranks + " of " + this.suits;
	}
	//calculate 
	public double calculateScore(){

		double rankScore = this.ranks.getScore();
		double suitsScore = this.suits.getScore();	
	// the ranks and suits already possess a value in them so we only need to call their score
		double totalPoints = rankScore + suitsScore;
			return totalPoints;
	}
}   